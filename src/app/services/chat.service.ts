import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

import { ChatMessage } from '../models/chat-message.model';


@Injectable()
export class ChatService {
  user: firebase.User;
  chatMessages: FirebaseListObservable<ChatMessage[]>;
  chatMessage: ChatMessage;
  userName: Observable<string>;
  myTalks: FirebaseListObservable<any[]>;
  invitedTalks: FirebaseListObservable<any[]>;

  constructor(private afdb: AngularFireDatabase,
              private afauth: AngularFireAuth) {
    this.afauth.authState.subscribe(auth => {

      if (auth !== undefined && auth !== null) {
        this.user = auth;
      }
      this.myTalks = this.getMyTalks();
      this.invitedTalks = this.getInvitedTalks();
      this.getUser().subscribe(a => {
        this.userName = a.displayName;
      });
    });
  }

  getUser() {
    return this.afdb.object(`/users/${this.user.uid}`);
  }

  getUsers() {
    return this.afdb.list('/users');
  }
  sendMessage(message: string) {
    const timestamp = this.getTimeStamp();
    const email = this.user.email;

    this.chatMessages = this.getMessages();
    console.log('Send to the console');
    this.chatMessages.push({
      message: message,
      timeSent: timestamp,
      userName: this.userName,
      email: email
    });
   }

   getTimeStamp(){
    const now = new Date();
    const date = now.getUTCDate() + '/' +
                  (now.getUTCMonth() + 1) + '/' +
                  now.getUTCFullYear();
    const time = now.getUTCHours() + ':' +
                  (now.getUTCMinutes() + 1) + ':' +
                  now.getUTCSeconds();
    return date + ' ' + time;
   }

   getMessages(): FirebaseListObservable<ChatMessage[]> {
    return this.afdb.list('messages', {
      query: {
        limitToLast: 25,
        orderByKey: true
      }
    });

   }

   getMyTalks() {
      return this.afdb.list(`/users/${this.user.uid}/myTalks`);
   }
   getInvitedTalks() {
      return this.afdb.list(`/users/${this.user.uid}/invitedTalks`);
   }

   addNewTalk(users: any[]) {
     const key = this.afdb.list('talks/').push({
       owner: this.user.uid
     }).key;
     for (let i = 0; i < users.length; i++) {
       this.afdb.list(`/talks/${key}`).push({
      id: users[i]
      });
     }
   }
}
